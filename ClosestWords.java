/* Labb 2 i DD1352 Algoritmer, datastrukturer och komplexitet    */
/* Se labbanvisning under kurswebben https://www.kth.se/social/course/DD1352 */
/* Ursprunglig författare: Viggo Kann KTH viggo@nada.kth.se      */
import java.util.LinkedList;
import java.util.List;
public class ClosestWords {
  LinkedList<String> closestWords = null;
  int[][] m;
  String oldWord = "";
  int closestDistance = -1;
  int startIndex;
 
 
  int partDist(String w1, String w2, int w1len, int w2len) {
    if (w1len == 0)
      return w2len;
    if (w2len == 0)
      return w1len;
    int res = m[w1len-1][w2len-1] + 
    (w1.charAt(w1len - 1) == w2.charAt(w2len - 1) ? 0 : 1);
    int addLetter = m[w1len-1][w2len] + 1;
    
    int deleteLetter = m[w1len][w2len - 1] + 1;
    
    return Math.min(res, Math.min(addLetter,deleteLetter));
  }
  /**
   * Memoization - step 1
   * Initializes an array for every new String in wordList
   * @param w1
   * @param w2
   */
  void alignment_init_first_row(String w1) {
    for(int i = 0; i<w1.length()+1; i++) {
      m[i][0] = i;
    }
  }
 
  void alignment_init_first_column(String w2) {
    for(int j = 0; j<50; j++) {
      m[0][j] = j;
    }
  }
  void alignment_finish(String w1, String w2) {
    //int startIndex = getDiff(oldWord, w2);
    for(int j = (startIndex == 0 ? 1 : startIndex+1); j<w2.length()+1; j++) {
      //System.out.println(j);
      for(int i = 1; i<w1.length()+1; i++) {
        m[i][j] = partDist(w1,w2,i,j);
      }
    }
  }
  /**
   * Takes two strings and returns the index at which they start to differ from each other.
   * @param  oldWord [The last word we used]
   * @param  newWord [New word to be compared with the old word]
   * @return         [index]
   */
  int getDiff(String oldWord, String newWord) {
    int count = 0;
    int limit = 0;
    limit = Math.min(oldWord.length(), newWord.length());
    for(int i = 0; i < limit; i++) {
      if(oldWord.charAt(i) != newWord.charAt(i)) {
        break;
      } else {
        count++;
      }

    }
    return count;
  }
 
  public ClosestWords(String w, List<String> wordList) {
    m = new int[w.length()+1][50];
    alignment_init_first_row(w);
    alignment_init_first_column("");

    for (String s : wordList) {
  
      if(Math.abs(w.length()-s.length()) > closestDistance && closestDistance != -1) {
        continue;
      } else {
        startIndex = getDiff(oldWord, s);
        alignment_finish(w, s);
        int dist = m[w.length()][s.length()];
        // System.out.println("d(" + w + "," + s + ")=" + dist);
        if (dist < closestDistance || closestDistance == -1) {
          closestDistance = dist;
          closestWords = new LinkedList<String>();
          closestWords.add(s);
        }
        else if (dist == closestDistance) {
          closestWords.add(s);
        }
        oldWord = s;
      }
    }
  }
 
  int getMinDistance() {
    return closestDistance;
  }
  List<String> getClosestWords() {
    return closestWords;
  }
}